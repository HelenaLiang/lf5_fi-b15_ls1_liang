public class Arbeitsblatt_2_Auf1_2_ {

	public static void main(String[] args) {
		// Aufgabe 1
		String ss = ("**");
		String s = ("*");
		System.out.printf("Aufgabe 1\n");
		System.out.printf("\n%5s",ss);
		System.out.printf("\n%s %6s ",s, s);
		System.out.printf("\n%s %6s ",s, s);
		System.out.printf("\n%5s\n",ss);
		
		//Aufgabe 2
		String sss = "1 * 2 * 3 * 4 * 5";
		System.out.printf("\nAufgabe 2\n");
		System.out.printf("\n%-5s = %-18.0s = %4d\n", "0!", sss, 1);
		System.out.printf("%-5s = %-18.1s = %4d\n", "1!", sss, 1);
		System.out.printf("%-5s = %-18.5s = %4d\n", "2!", sss, 1 * 2);
		System.out.printf("%-5s = %-18.9s = %4d\n", "3!", sss, 1 * 2 * 3);
		System.out.printf("%-5s = %-18.13s = %4d\n", "4!", sss, 1 * 2 * 3 * 4);
		System.out.printf("%-5s = %-18.17s = %4d\n", "5!", sss, 1 * 2 * 3 * 4 * 5);
		


	}

}
