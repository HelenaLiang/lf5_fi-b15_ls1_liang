﻿//Aufgabe 2.4
import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       double eingeworfeneMünze;
       double rückgabebetrag;

       
       //einen neue Linie hinzugefügt das Ticketpreis und Anzahl ausgibt*
       zuZahlenderBetrag = fahrkartenbestellungErfassen();

       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = 0.0;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
       {
    	   System.out.printf("Noch zu zahlen: %.2f Euro "  , (zuZahlenderBetrag - eingezahlterGesamtbetrag));
    	   
           eingezahlterGesamtbetrag += fahrkartenBezahlen(zuZahlenderBetrag);
       }

       //Animation zur Fahrscheinausgabe
       fahrkartenAusgeben();

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       rueckgeldAusgeben(rückgabebetrag);
     
       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
    }
    // Methode dass das Gesamtpreis returnt und druckt "Ticketpreis"&"Anzahl"
    public static double fahrkartenbestellungErfassen() {
    	double preis;
    	int anzahlDerTickets;
    	// Eingabe für den Ticketpreis
        System.out.println("Ticketpreis (EURO): ");
        Scanner tastatur = new Scanner(System.in);
        preis = tastatur.nextDouble();
        
     
        System.out.println("Bitte wählen Sie die Anzahl der gewünschten Tickets.\nHinweis! Maximale Anzahl beträgt 10 Tickets");
        
        //Eingabe für die Ticketanzahl
        anzahlDerTickets = tastatur.nextInt();
        System.out.println("\nAnzahl der Tickets: ");
        //Bedingung wenn Kunde weniger als 1 und mehr als 10 eingibt, defult 1 Ticket
        if (anzahlDerTickets <= 0 || anzahlDerTickets>10) {
    		System.out.println("Fehler! Anzhal ist ungültig. Es wird nur 1 ticket gedruckt");
    		anzahlDerTickets = 1;
    	}
        //bei negativen TP, wird auf 1€ gesetzt
        if(preis < 0) {
        	System.out.println("Fehler! Ticketpreis wir auf 1€ gesetzt");
        	preis = 1;
        }
      
        //neuer preis = alter preis(pro Ticket) * anzahl(tastur.nextInt())
        preis = preis * anzahlDerTickets;
    	return preis;
    	
    	
    }
    // Methode nimmt den Wert was eingeworfen wird
    public static double fahrkartenBezahlen(double ZuZahlen) {
    	Scanner tastatur = new Scanner(System.in);
    	
    	System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    	//deklarierung und initialisierung in einem!!
    	double eingeworfeneMünze = tastatur.nextDouble();
    	
		return eingeworfeneMünze;  	
    }
    // Methode um zu Warten
    public static void warte(int millisekunde) {
    	try {
 			Thread.sleep(millisekunde);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
    }
    // Methode für die Animation ======
    public static void fahrkartenAusgeben() {
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           warte(250);
        }
        System.out.println("\n\n");

    }
    // Methode formatiert die Ausgabe des Rückgelds
    public static void muenzeAusgeben(int betrag, String einheit) {
    	
    	System.out.printf("\n%2d %s\n",betrag, einheit);
    }
    
    //Das gibt mir mein Rückgeld
    public static void rueckgeldAusgeben(double rueckgeld) {
    	  if(rueckgeld > 0.0)
          {
       	   System.out.println("Der Rückgabebetrag in Höhe von " + rueckgeld + " EURO");
       	   System.out.println("wird in folgenden Münzen ausgezahlt:");

              while(rueckgeld >= 2.0) // 2 EURO-Münzen
              {
            	  muenzeAusgeben(2,"EURO");
            	  rueckgeld -= 2.0;
              }
              while(rueckgeld >= 1.0) // 1 EURO-Münzen
              {
            	  muenzeAusgeben(1,"EURO");
            	  rueckgeld -= 1.0;
              }
              while(rueckgeld >= 0.5) // 50 CENT-Münzen
              {
            	  muenzeAusgeben(50,"CENT");
            	  rueckgeld -= 0.5;
              }
              while(rueckgeld >= 0.2) // 20 CENT-Münzen
              {
            	  muenzeAusgeben(20,"CENT");
    	          rueckgeld -= 0.2;
              }
              while(rueckgeld >= 0.1) // 10 CENT-Münzen
              {
            	  muenzeAusgeben(10,"CENT");
            	  rueckgeld -= 0.1;
              }
              while(rueckgeld >= 0.05)// 5 CENT-Münzen
              {
            	  muenzeAusgeben(5,"CENT");
    	          rueckgeld -= 0.05;
              }
          }

    }
}