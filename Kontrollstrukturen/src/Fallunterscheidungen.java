import java.util.Scanner;

public class Fallunterscheidungen {
	


	public static void main(String[] args) {
	//Methoden aufrufen
	schulNoten();
	}
	// 1 Noten
	public static void schulNoten() {
		// Program: Notenausgabe
		// Scanner integrieren
		Scanner myScanner = new Scanner(System.in);
		 
		int note;
		System.out.println("Was ist die Note?");
		note = myScanner.nextInt();
		
		switch(note) 
		{
			case 1:
				System.out.println("Sehr gut");
				break;
			case 2:
				System.out.println("Gut");
				break;
			case 3:
				System.out.println("Befriedigend");
				break;
			case 4:
				System.out.println("Ausreichend");
				break;
			case 5:
				System.out.println("Mangelhaf");
				break;
			case 6:
				System.out.println("Ungen�gend");
				break;
			default:
				System.out.println("Fehler");
		}
	}
	public static void monat() {
		// Program: Monat zur�ckgeben
			// Scanner integrieren
			Scanner myScanner = new Scanner(System.in);
				 
			int monat;
			System.out.println("Geben Sie Bitte eine Monatszahl an?");
			monat = myScanner.nextInt();
				
			switch(monat) 
				{
				case 1:
					System.out.println("Januar");
					break;
				case 2:
					System.out.println("Februar");
					break;
				case 3:
					System.out.println("M�rz");
					break;
				case 4:
					System.out.println("April");
					break;
				case 5:
					System.out.println("Mai");
					break;
				case 6:
					System.out.println("Juni");
					break;
				case 7:
					System.out.println("Juli");
					break;
				case 8:
					System.out.println("August");
					break;
				case 9:
					System.out.println("September");
					break;
				case 10:
					System.out.println("Oktober");
					break;
				case 11:
					System.out.println("November");
					break;
				case 12:
					System.out.println("Dezember");
					break;
				default:
					System.out.println("Fehler");
				}
	}
	public static void roemischeZahlen() {
		// Program: Rom
					// Scanner integrieren
					Scanner myScanner = new Scanner(System.in);
					 
					char rom;
					System.out.println("Gebe ein R�misches Zahlzeichen ein?");
					rom = myScanner.next().charAt(0);
					
					switch(rom) 
					{
						case 'I':
							System.out.println(1);
							break;
						case 'V':
							System.out.println(5);
							break;
						case 'X':
							System.out.println(10);
							break;
						case 'L':
							System.out.println(50);
							break;
						case 'C':
							System.out.println(100);
							break;
						case 'D':
							System.out.println(500);
							break;
						case 'M':
							System.out.println(1000);
							break;
						default:
							System.out.println("Fehler");
			}
	}
	
}
