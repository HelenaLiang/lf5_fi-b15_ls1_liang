import java.util.Scanner;

public class FallunterscheidungenNote {

	public static void main(String[] args) {
		// Program: Notenausgabe
		// Scanner integrieren
		Scanner myScanner = new Scanner(System.in);
		 
		int note;
		System.out.println("Was ist die Note?");
		note = myScanner.nextInt();
		
		switch(note) 
		{
			case 1:
				System.out.println("Sehr gut");
				break;
			case 2:
				System.out.println("Gut");
				break;
			case 3:
				System.out.println("Befriedigend");
				break;
			case 4:
				System.out.println("Ausreichend");
				break;
			case 5:
				System.out.println("Mangelhaf");
				break;
			case 6:
				System.out.println("Ungenügend");
				break;
			default:
				System.out.println("Fehler");
		}
	}

}
